FROM coala/base:0.9
MAINTAINER Lasse Schuirmann <lasse.schuirmann@gmail.com>

RUN mkdir /app
WORKDIR /app

# Just so it builds faster when changing app code but not requirements
COPY backend/requirements.txt /app
RUN pip install -r requirements.txt
COPY backend /app

ENV BEAR_CRON_TIME='*/200 * * * *' REPO_CRON_TIME='*/200 * * * *' CONTRIBUTORS_CRON_TIME='*/200 * * * *' USE_DB_CACHE=True ISSUES_CRON_TIME='*/200 * * * *' MRS_CRON_TIME='*/200 * * * *'

RUN python3 manage.py migrate
RUN python3 manage.py createcachetable
RUN python3 manage.py crontab add

CMD gunicorn coala_web.wsgi -b 0.0.0.0:8000
