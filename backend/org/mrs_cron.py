from functools import lru_cache

from org.git import get_igitt_org


@lru_cache(maxsize=32)
def get_mrs(hoster):
    data = dict()
    org = get_igitt_org(hoster)
    repos = org.repositories
    for repo in repos:
        mrs = repo.filter_merge_requests(state='all')
        for mr in mrs:
            data[mr.number] = {
                'number': mr.number,
                'title': mr.title,
                'description': mr.description,
                'repo': repo.full_name,
                'url': mr.web_url,
                'closes_issues': (
                    [int(issue.number) for issue in mr.closes_issues]),
                'state': mr.state.value,
                'author': mr.author.username,
                'assignees': [assignee.username for assignee in mr.assignees],
                'ci_status': mr.tests_passed,
                'created_at': mr.created,
                'updated_at': mr.updated,
                'labels': [label for label in mr.labels],
                'reactions': [r.name for r in mr.reactions],
            }
    return data
