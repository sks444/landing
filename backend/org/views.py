import json

from django.views.decorators.csrf import csrf_exempt
from django.views.decorators.http import require_http_methods
from django_common.http import HttpResponse
from org.models import Contributor
from org.issues_cron import get_issues
from org.mrs_cron import get_mrs

from brake.decorators import ratelimit


@ratelimit(block=True, rate='50/m')
@csrf_exempt
@require_http_methods(['GET'])
def contrib(request):
    response = Contributor.objects.all()
    data = [{'name': item.name,
             'issues': item.issues_opened,
             'contributions': item.num_commits,
             'bio': item.bio,
             'login': item.login,
             'reviews': item.reviews,
             'teams': [team.name for team in item.teams.all()],
             } for item in response]
    return HttpResponse(json.dumps(data), content_type='application/json')


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def issues(request, hoster):
    issues = get_issues(hoster)
    # param default=str is used to dump the datetime object into string.
    return HttpResponse(
        json.dumps(issues, indent=4, default=str),
        content_type='application/json',)


@ratelimit(block=True, rate='1/m')
@csrf_exempt
@require_http_methods(['GET'])
def merge_requests(request, hoster):
    mrs = get_mrs(hoster)
    return HttpResponse(
        json.dumps(mrs, indent=4, default=str),
        content_type='application/json',)
