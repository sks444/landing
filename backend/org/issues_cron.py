from functools import lru_cache

from org.git import get_igitt_org


@lru_cache(maxsize=32)
def get_issues(hoster):
    data = dict()
    org = get_igitt_org(hoster)
    repos = org.repositories
    for repo in repos:
        issues = repo.filter_issues(state='all')
        for issue in issues:
            data[issue.number] = {
                'number': issue.number,
                'title': issue.title,
                'description': issue.description,
                'author': issue.author.username,
                'created_at': issue.created,
                'updated': issue.updated,
                'state': issue.state,
                'repo': repo.full_name,
                'reactions': [r.name for r in issue.reactions],
                'labels': [label for label in issue.labels],
                'mrs_closed_by': [int(i.number) for i in issue.mrs_closed_by],
            }
    return data
